# Radar Tecnológico Individual

### Usando actualmente
---
| Lenguajes | Plataformas | Herramientas | Técnicas |   
|---|---|---|---|
| C# | Unity  |  Tiled |   |   
| Java |   |   |   |   
 
### Radar
---
|   | Adoptar | Probar | Evaluar | Evitar |
|---|---|---|---|---|
| Lenguajes | Scala, Ember.js, React.js | Ruby | Dapper | Smalltalk |
| Plataformas | Docker | Auth0, UnrealEngine 4 | Electron  | - |
| Herramientas |  - |  - | Babel, JSON Assert  | - |
| Técnicas | - | - | - | - |